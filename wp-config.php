<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '123456' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*TNXw3@Z{=~gN~>g,h<*pJwAmR^ow_5qsH{(37[Z[Cy13O?peLZ1hw+GS&(H93] ' );
define( 'SECURE_AUTH_KEY',  '0*(.E3iJ$,L}-gvv>p}0k!dQ>;t5PDzGYtWFn!=o<InRr3R?^-4gO-K9i_z),t)u' );
define( 'LOGGED_IN_KEY',    '9ZC{P#jl-}D|w#oNJeX*|H+bA6:T]i{,mBL}9+<DL|E5f$jE4Q8boJta4,JLS#5C' );
define( 'NONCE_KEY',        'H=d:B,zTNn!Z~tb0uTb~r66^0lz%v^#p6tQ&41EY2+~0>!Awmbi[gSBIdp5*]w4F' );
define( 'AUTH_SALT',        '`=Vx_76y&`<x3<ivR1QISYb@8?n8w0?OiB~(F(KI(j2elUK<wR4n>Gi_#1K<nZ,%' );
define( 'SECURE_AUTH_SALT', '$eGuI#0{_M_^9xWv-Rh2yau,{.kF?B/-P`Tq>3zfw56/VgGrkB8t$kQ*c:eQS8|s' );
define( 'LOGGED_IN_SALT',   'W7ii2__*04w}[q`e1 M++b__@6A4,Hz@tl?/uRTeY^HO7<(4G7?&[3IO~,d79+Yy' );
define( 'NONCE_SALT',       '2B[bFRrjVc!t!;-ggTR8a+yM$ B#*o+Fj#/3V+j9Bb3CpfVjpd`E#~UjrSC(dVxx' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
